<?php

namespace Core;

class CommandController
{
    const RED_BG = "\e[41m";
    const BLUE_BG = "\e[44m";
    const GREEN_BG = "\e[42m";

    public function message($text, $prefix = '')
    {
        echo $prefix . $text . "\e[0m" . PHP_EOL;
    }
}
