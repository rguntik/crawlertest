<?php

namespace Core;

abstract class Controller
{
    public $name;
    protected $layout = 'main';
    protected $view;
    public $config;

    public function __construct()
    {
        $this->view = new View($this->setName());
        $this->view->title = $this->name;
        $this->config = rgConfig::getInstance();

        return $this;
    }

    protected function setName()
    {
        $name = (new \ReflectionClass($this))->getShortName();
        $this->name = strtolower(substr($name, 0, -10));

        return $this->name;
    }

    public function render($view, $params = [])
    {
        $this->view->layout = $this->layout;

        return $this->view->render($view, $params);
    }
    
    public function redirect($path = '')
    {
        header('Location: ' . App::getConfig('main')['baseUrl'] . $path);
        exit;
    }
}
