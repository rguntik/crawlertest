<?php

namespace Core;

class rgConfig
{
    private static $instance = null;
    private $configFolder;

    private function __construct($configFolder = null)
    {
        $this->configFolder = $configFolder;
        $this->init();

        return $this;
    }

    private function init()
    {
        foreach (scandir($this->configFolder) as $val) {
            if (preg_match('/(.*)\.php/', $val, $match)) {
                $confArr = require($this->configFolder . '/' . $val);
                if (is_array($confArr)) {
                    $confName = $match[1];
                    $this->$confName = $confArr;
                }
            }
        }
    }

    static public function getInstance($configFolder = null)
    {
        if (is_null(self::$instance)) {
            self::$instance = new self($configFolder);
        }

        return self::$instance;
    }

    protected function __clone()
    {
    }
}
