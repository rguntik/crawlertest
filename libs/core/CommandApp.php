<?php

namespace Core;

class CommandApp
{
    private static $instance = null;
    private $request;
    protected $config;
    private $controller;

    private function __construct()
    {
        $this->config = rgConfig::getInstance();
        $this->initRequest()->dispatch();

        return $this;
    }

    private function initRequest()
    {
        $argv = $GLOBALS['argv'];
        $request = $this->config->main['requestDefault'];
        $request['actionParams'] = [];

        if (array_key_exists(1, $argv)) {
            $request['controller'] = $argv[1];
        }

        if (array_key_exists(2, $argv)) {
            $request['action'] = $argv[2];
            if (count($argv) > 3) {
                array_splice($argv, 0, 3);
                $request['actionParams'] = $argv;
            }
        }

        foreach (['controller', 'action'] as $val) {
            $request[$val] = ucfirst($request[$val]) . ucfirst($val);
        }

        $this->request = $request;

        return $this;
    }

    static public function getConfig($name = null)
    {
        $result = self::getInstance()->config;
        if ($name) {
            $result = (property_exists($result, $name)) ? $result->$name : null;
        }

        return $result;
    }

    static public function getInstance()
    {
        if (is_null(self::$instance)) {
            $self = new self();
            self::$instance = $self;
        }

        return self::$instance;
    }


    private function dispatch()
    {
        $controllerName = "App\\Commands\\" . $this->request["controller"];
        try {
            $controllerObj = new $controllerName();
        } catch (\Exception $e) {
           throw new \Exception('Command not found');
        }

        if (!method_exists($controllerObj, $this->request['action'])) {
            throw new \Exception("Action not found");
        }

        $this->controller = $controllerObj;
    }

    public function run()
    {
        call_user_func_array(
            [
                $this->controller,
                $this->request['action']
            ],
            $this->request['actionParams']
        );
    }

    protected function __clone()
    {
    }
}
