<?php

namespace Core;

class App
{
    private static $instance = null;
    protected $config;
    private $request;
    private $controller;

    private function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        return $this;
    }

    static public function getConfig($name = null)
    {
        $result = self::getInstance()->config;
        if ($name) {
            $result = (property_exists($result, $name)) ? $result->$name : null;
        }

        return $result;
    }

    static public function getInstance()
    {
        if (is_null(self::$instance)) {
            $self = new self();
            self::$instance = $self;
            $self->init();
        }

        return self::$instance;
    }

    private function init()
    {
        $this->config = rgConfig::getInstance();
        $this->initRequest();
        $this->dispatch();
    }

    private function initRequest()
    {
        $request = $this->config->main['requestDefault'];
        $request['actionParams'] = [];
        $request['isPost'] = $_SERVER['REQUEST_METHOD'] === 'POST';
        $request['params'] = [
            'get' => $_GET,
            'post' => $_POST,
        ];

        $requestUri = strtok($_SERVER['REQUEST_URI'], '?');
        $request['URI'] = $requestUri;
        if ($requestUri != '/') {
            $requestUri = explode('/', substr($requestUri, 1));
            $paramsCount = count($requestUri);
            $request['controller'] = $requestUri[0];
            if ($paramsCount > 1) {
                $request['action'] = $requestUri[1];
            }
            if ($paramsCount > 2) {
                array_splice($requestUri, 0, 2);
                $request['actionParams'] = $requestUri;
            }
        }

        foreach (['controller', 'action'] as $val) {
            $request[$val] = ucfirst($request[$val]) . ucfirst($val);
        }

        $this->request = $request;
    }

    private function dispatch()
    {
        $request = $this->getRequest();
        $controllerName = "App\\Controllers\\" . $request["controller"];
        $controllerObj = new $controllerName();
        if (!method_exists($controllerObj, $request['action'])) {
            throw new \Exception("Controller Error");
        }
        $this->controller = $controllerObj;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function run()
    {
        $request = $this->getRequest();
        $result = call_user_func_array([$this->controller, $request['action']], $request['actionParams']);

        echo $result;
    }

    protected function __clone()
    {
    }
}
