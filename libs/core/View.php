<?php

namespace Core;

class View
{
    public $layout = null;
    public $mainFolder;
    public $layoutFolder;
    public $viewFolder;
    public $asset = 'default';
    protected $assetConf = [
        'path' => '',
        'js' => [],
        'css' => [],
    ];

    public function __construct($viewFolder = null)
    {
        $this->initAssets();
        $this->pathInit();
        if ($viewFolder) {
            $this->setViewFolder($viewFolder);
        }

        return $this;
    }

    protected function initAssets()
    {
        $config = App::getConfig('assets');
        if (array_key_exists($this->asset, $config)) {
            $config = $config[$this->asset];
            $this->assetConf['path'] = App::getConfig('main')['baseUrl'] . $config['path'];

            foreach (['css', 'js'] as $type) {
                array_walk($config[$type], function ($v, $k) use ($type) {
                    $this->registrateAsset($v, $type);
                });

            }
        } else {
            throw new \Exception("Asset not found.");
        }
    }

    public function registrateAsset($file, $type, $usePath = true)
    {
        $path = (preg_match('/^http/', $file) || !$usePath) ? '' : $this->assetConf['path'];
        $this->assetConf[$type][] = $path . $file;
    }

    protected function pathInit()
    {
        $config = App::getConfig();
        $this->mainFolder = $config->main['rootPath'] . $config->view['folder'];
        $this->layoutFolder = $this->mainFolder . $config->view['layoutFolder'];
        $this->viewFolder = $this->mainFolder;
    }

    public function setViewFolder($folder)
    {
        $this->viewFolder = $this->mainFolder . '/' . $folder;
    }

    public function render($view, $params = [])
    {
        $file = $this->viewFolder . '/' . $view . '.php';
        $content = $this->renderFile($file, $params);

        if ($this->layout) {
            $content = $this->renderFile($this->getLayoutPath(), [
                'content' => $content,
                'scripts' => $this->renderFile($this->mainFolder . '/scripts.php', [
                    'asset' => $this->assetConf
                ])
            ]);
        }

        return $content;
    }

    protected function renderFile($path, $params = [])
    {
        if (!is_file($path)) {
            throw new \Exception("file $path not found.");
        }

        $this->params = $params;
        ob_start();
        include $path;
        $content = ob_get_clean();
        if (ob_get_contents()) ob_end_flush();

        return $content;
    }

    public function getLayoutPath()
    {
        $path = $this->layoutFolder . '/' . $this->layout . '.php';

        return $path;
    }
}
