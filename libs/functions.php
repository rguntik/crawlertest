<?php

if (!function_exists('dump')) {
    function dump($var) {
        foreach (func_get_args() as $var) {
            try {
                Symfony\Component\VarDumper\VarDumper::dump($var);
            } catch (Exception $e) {
                echo '<pre>';
                var_dump($var);
                echo '</pre>';
            }
        }
    }
}

function arrayHasNext(array $a){
    return next($a) !== false ?: each($a) !== false;
}
