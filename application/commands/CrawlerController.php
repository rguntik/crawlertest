<?php

namespace App\Commands;

use App\Helper\ImageCrawler;
use Core\{
    rgConfig,
    CommandController
};

class CrawlerController extends CommandController
{
    public function parseAction(string $url = 'http://mnemogenerator.com', int $pagesCount = 100)
    {
        $config = rgConfig::getInstance();

        $crawler = new ImageCrawler($url, $pagesCount);
        $reportData = $crawler->parse();

        $fileName = 'report_' . date('d.m.Y_H:i:s');
        $reportDit = $config->main['reportPath'];
        $this->checkDir($reportDit);

        array_unshift($reportData, ['Url', 'Image count', 'Time']);
        $reportDataHtml = $this->getDataHtml($reportData, $fileName);

        $reportPath = "$reportDit/$fileName.html";
        file_put_contents($reportPath, $reportDataHtml);

        $this->message("Parsed url: $url", self::GREEN_BG);
        $this->message("Page count: " . (count($reportData) - 1), self::BLUE_BG);
        $this->message("Report path: " . $reportPath, self::BLUE_BG);
    }

    protected function checkDir($path)
    {
        return is_dir($path) ?: @mkdir($path, 0777, true);
    }

    /**
     * @param array $data
     * @param string $titleStr
     * @return string
     */
    protected function getDataHtml(array $data, string $titleStr = "")
    {
        $doc = new \DOMDocument('1.0');

        $root = $doc->createElement('html');
        $root = $doc->appendChild($root);

        $head = $doc->createElement('head');
        $head = $root->appendChild($head);

        $title = $doc->createElement('title');
        $title = $head->appendChild($title);

        $title->appendChild($doc->createTextNode($titleStr));

        $body = $doc->createElement('body');
        $body = $root->appendChild($body);

        $table = $doc->createElement('table');
        $table = $body->appendChild($table);
        $table->setAttribute("border", 1);
        $table->setAttribute("align", "center");
        $table->setAttribute("width", "100%");

        foreach ($data as $key => $row) {
            $tr = $doc->createElement('tr');
            $tr = $table->appendChild($tr);
            if (!$key) {
                $tr->setAttribute("align", "center");
            }

            foreach ($row as $val) {
                $td = $doc->createElement('td');
                $td = $tr->appendChild($td);
                $td->appendChild($doc->createTextNode($val));
            }
        }

        return $doc->saveHTML();
    }
}
