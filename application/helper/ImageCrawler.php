<?php

namespace App\Helper;

class ImageCrawler
{
    private $pageCount;
    private $siteUrl;
    private $storage = [];
    private $dom;
    private $pattern;

    /**
     * ImageCrawler constructor.
     * @param string $url
     * @param int $maxCountPage
     * @throws \Exception
     */
    public function __construct(string $url, int $maxCountPage = null)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) == false) {
           throw new \Exception('Not valid url');
        }

        if (!$maxCountPage || $maxCountPage < 0) {
           $maxCountPage = null;
        }

        $urlComponents = parse_url($url);

        $this->siteUrl = "{$urlComponents['scheme']}://{$urlComponents['host']}";

        $key = array_key_exists('path', $urlComponents) ? $urlComponents['path'] : '/';
        $this->storage[$key] = [];

        $this->pageCount = $maxCountPage;
        $this->dom = new \DOMDocument();
        $this->pattern = '/^(\/|' . preg_quote($this->siteUrl, '/') . ')/';

        return $this;
    }

    public function parse()
    {
        $result = [];
        $sortArr = [];
        libxml_use_internal_errors(true);
        $this->parsePage(key($this->storage));
        foreach ($this->storage as $url => $val) {
            array_unshift($val, $this->siteUrl . $url);
            $result[] = $val;
            $sortArr[] = $val[1];

        }
        array_multisort($sortArr, $result);

        return $result;
    }

    private function parsePage($url)
    {
        if ($this->pageCount !== null) {
           $this->pageCount -= 1;
        }
        $time = microtime(true);
        $content = $this->getPageContent($this->siteUrl . $url);

        if ($content) {
            $this->dom->loadHTML($content);
            $links = $this->dom->getElementsByTagName('a');
            $this->storage[$url] = [
                $this->dom->getElementsByTagName('img')->length,
                microtime(true) - $time
            ];

            if ($links->length) {
                foreach ($links as $link) {
                    /** @var \DOMElement $link */
                    $href = $link->getAttribute('href');
                    if (preg_match($this->pattern, $href) && !array_key_exists($href, $this->storage)) {
                        if ($this->pageCount !== null && $this->pageCount || $this->pageCount === null) {
                            $this->parsePage($href);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param string $url
     * @return string|null
     */
    private function getPageContent($url)
    {
        $result = null;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200) {
           $result = $output;
        }
        curl_close($ch);

        return $result;
    }
}
