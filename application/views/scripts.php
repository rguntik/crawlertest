<? foreach (['css', 'js'] as $type): ?>
    <? foreach ($this->params['asset'][$type] as $val): ?>
        <? if ($type == 'js') :?>
            <script src="<?= $val . '?' . time(); ?>"></script>
        <? else : ?>
            <link href="<?= $val . '?' . time(); ?>" rel="stylesheet">
        <? endif; ?>
    <? endforeach; ?>
<? endforeach; ?>
