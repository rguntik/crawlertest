<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $this->title; ?></title>
    <?= $this->params['scripts']; ?>
</head>

<body>

<!-- Begin page content -->
<div class="container">
    <?= $this->params['content']; ?>
</div>

<footer class="footer">
    <div class="container">
    </div>
</footer>
</body>
</html>

