<h1>Crawler</h1>
<h1>
    <h3>Controller path:</h3>
    <? echo($params['controllerPath']); ?>
    <h3>View path:</h3>
    <? echo(__FILE__); ?>
</h1>
<div>
    <ul>
        <? foreach ($params['reports'] as $link) : ?>
            <li>
                <a href="/reports/<?= $link; ?>" target="_blank"><?= $link; ?></a>
            </li>
        <? endforeach; ?>
    </ul>
</div>
