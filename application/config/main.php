<?php

$rootPath = $_SERVER['DOCUMENT_ROOT'];

return [
    'rootPath' => $rootPath,
    'baseUrl' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'],
    'reportPath' => "$rootPath/reports",
    'requestDefault' => [
        'controller' => 'test',
        'action' => 'index',
    ],
];
