<?php

$rootPath = realpath(__DIR__ . '/../../..');

return [
    'rootPath' => $rootPath,
    'reportPath' => "$rootPath/reports",
    'requestDefault' => [
        'controller' => 'crawler',
        'action' => 'parse',
    ],
];
