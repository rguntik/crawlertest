<?php

$rootPath = $_SERVER['DOCUMENT_ROOT'];

return [
    'rootPath' => $rootPath,
    'libPath' => $rootPath . '/libs',
];
