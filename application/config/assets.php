<?php

return [
    'default' => [
        'path' => '/assets',
        'js' => [
            'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js',
            '/js/test.js'
        ],
        'css' => [
            '/css/style.css',
        ]
    ],
    'empty' => []
];
