<?php

namespace App\Controllers;

use Core\Controller;

class TestController extends Controller
{
    public function indexAction()
    {
        $reportDit = $this->config->main['reportPath'];
        $list = is_dir($reportDit) ? array_slice(scandir($reportDit), 2) : [];

        return $this->render('index', [
            'controllerPath' => __FILE__,
            'reports' => $list
        ]);
    }
}
