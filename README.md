### Requires:

* PHP >= 7.0 
* Curl extension
* You must enable short_open_tag in your php.ini

#####Description:
To run test script you must to go in the root folder of the project and enter one of the commands:
```sh
$ php bin/console
$ php bin/console crawler
$ php bin/console crawler parse
$ php bin/console crawler parse http://mnemogenerator.com
$ php bin/console crawler parse http://mnemogenerator.com 10 
```

```sh
$ php bin/console {param1} {param2} {param3} {param4} 
```

* **param1** - controller name (default 'crawler') 
* **param2** - action name (default 'parse') 
* **param3** - url for parsing(default 'http://mnemogenerator.com') 
* **param4** - count page for parsing(default 100) 

You can find result in report directory or you can create virtual host for project and find all links on the main page
