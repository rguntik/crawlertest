<?php

function Loader($className)
{
    $nameSpace = getLoadPath($className);
    if ($nameSpace) {
        $classPath = $nameSpace['folder'];

        foreach (explode("\\", $nameSpace['class']) as $val) {
            $path = $classPath . '/' . $val;
            if (is_dir($path)) {
                $classPath = $path;
            } elseif (is_file($path . '.php')) {
                $classPath = $path . '.php';
                break;
            } else {
                $valWithoutCamel = strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', $val));
                $pathWithoutCamel = $classPath . '/' . $valWithoutCamel;

                if ($valWithoutCamel != $val && is_dir($pathWithoutCamel)) {
                    $classPath = $pathWithoutCamel;
                } else {
                    $classPath = null;
                    break;
                }
            }
        }

        if ($classPath) {
            include_once $classPath;
        } else {
            throw new \Exception("class {$className} not found.");
        }
    }
}

function getLoadPath($className)
{
    $conf = [
        'Core' => '/libs/core',
        'App' => '/application',
        'Symfony\Component' => '/libs/symfony',
    ];

    foreach ($conf as $key => $val) {
        if (preg_match("/^($key)\\\(.*)/", $className, $match)) {
            return [
                'folder' => __DIR__ . $val,
                'class' => $match[2]
            ];
        }
    }

    return false;
}

