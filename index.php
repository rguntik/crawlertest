<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
use Core\App;
use Core\rgConfig;

include_once 'Loader.php';
spl_autoload_register('Loader');

$config = rgConfig::getInstance(__DIR__ . '/application/config');
include_once $config->web['libPath'] . '/functions.php';

$app = App::getInstance()->run();

